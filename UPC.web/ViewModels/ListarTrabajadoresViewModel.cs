﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.web.Models;
namespace UPC.web.ViewModels
{
    public class ListarTrabajadoresViewModel
    {
        public List<Usuario> ListEmpleados { get; set; }
        public List<Usuario> ListSupervisores { get; set; }
        public List<Usuario> ListProveedores { get; set; }
        public List<Usuario> ListAdministradores { get; set; }

        public ListarTrabajadoresViewModel()
        {
            TP_solucion4Entities context = new TP_solucion4Entities();
            ListEmpleados = context.Usuario.Where(x => (x.categoriaID == 1 || x.categoriaID == 5) && x.estado != "ELI").ToList();
            ListSupervisores = context.Usuario.Where(x => x.categoriaID == 2 && x.estado != "ELI").ToList();
            ListProveedores = context.Usuario.Where(x => x.categoriaID == 3 && x.estado != "ELI").ToList();
            ListAdministradores = context.Usuario.Where(x => x.categoriaID == 4 && x.estado != "ELI").ToList();
        }
       
    }
}