﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.web.Models;
namespace UPC.web.ViewModels
{
    public class AddEditInforme_LaborViewModel
    {

        public int? Informe_LaborId { get; set; }
        public int LaborID { get; set; }
        public int UsuarioId { get; set; }

        public int puntos_asignados { get; set; }
        public string comentario { get; set; }


        public DateTime fecha_culminacion { get; set; }
        public SelectList Labores { get; set; }
        public SelectList Empleados { get; set; }
        public int Persona_calificadaId { get; set; }

        public AddEditInforme_LaborViewModel()
        {
            TP_solucion4Entities context = new TP_solucion4Entities();
            Empleados = new SelectList(context.Usuario.Where(x=>x.categoriaID == 1 ), "usuarioID", "nombre");
            Labores = new SelectList(context.Labores, "laborID", "Descripcion");   
        }
        public void CargarDatos(int? Informe_laborID)
        {
            Informe_LaborId = Informe_laborID;
            if (Informe_LaborId.HasValue)
            {
                TP_solucion4Entities context = new TP_solucion4Entities();
                Informe_labor objInformeLabor = context.Informe_labor.FirstOrDefault(x => x.Informe_labor_ID == Informe_LaborId);
                this.LaborID = objInformeLabor.laborID;
                this.Persona_calificadaId = objInformeLabor.persona_calificadaID;
                this.puntos_asignados = objInformeLabor.puntos;
                this.comentario = objInformeLabor.comentario;
                this.UsuarioId = objInformeLabor.usuarioID;
                this.fecha_culminacion = objInformeLabor.Fecha_culminado;


            }
        }
    }
}