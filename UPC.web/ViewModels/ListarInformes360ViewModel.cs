﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.web.Models;
namespace UPC.web.ViewModels
{
    public class ListarInformes360ViewModel
    {
        public List<Informe360> ListarInfromes { get; set; }
        public ListarInformes360ViewModel()
        {

        }
        public void CargarDatos(int Id)
        {
            TP_solucion4Entities context = new TP_solucion4Entities();
            ListarInfromes = context.Informe360.Where(x => x.usuarioID == Id && x.estado!="INA").ToList();
        }
    }
}