﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.web.Models;
namespace UPC.web.ViewModels
{
    public class AddComunIntViewModel
    {
        public int AnuncioID { get; set; }
        public int UsuarioID { get; set; }

        public SelectList Usuarios { get; set; }
        public int RemitenteID { get; set; }

        public String Asunto { get; set; }
        public String Descripcion { get; set; }
        public DateTime fecha_publicacion { get; set; }

        public SelectList TipoAnuncio { get; set; }
        public int tipo_anuncioID { get; set; }

        public AddComunIntViewModel()
        {
            TP_solucion4Entities context = new TP_solucion4Entities();
            Usuarios = new SelectList(context.Usuario, "usuarioID", "nombre");
            TipoAnuncio = new SelectList(context.Tipo_anuncio.Where(x=>x.tipo_anuncioID != 3), "tipo_anuncioID", "nombre_tipo");
        }

    }
}