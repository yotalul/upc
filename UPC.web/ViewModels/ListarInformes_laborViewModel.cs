﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.web.Models;
namespace UPC.web.ViewModels
{
    public class ListarInformes_laborViewModel
    {

        public int UsuarioID;
        public List<Informe_labor> ListaInformes { get; set; }
        public DateTime? Rango1 { get; set; }
        public DateTime? Rango2 { get; set; }

        public ListarInformes_laborViewModel()
        {

        }
        public void CargarDatos()
        {
            if (Rango1.HasValue && Rango2.HasValue)
            {
                TP_solucion4Entities context = new TP_solucion4Entities();
                ListaInformes = context.Informe_labor.Where(x => x.Fecha_culminado >= Rango1 && x.Fecha_culminado <= Rango2 && x.usuarioID == UsuarioID).ToList();
            }
            else
            {
                TP_solucion4Entities context = new TP_solucion4Entities();
                ListaInformes = context.Informe_labor.Where(x => x.usuarioID == UsuarioID).ToList();
            }
        }


    }
}