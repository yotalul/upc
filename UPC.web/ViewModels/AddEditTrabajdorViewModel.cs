﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.web.ViewModels;
using UPC.web.Models;
namespace UPC.web.ViewModels
{
    public class AddEditTrabajdorViewModel
    {
        public int? UsuarioId { get; set; }
        public String Nombre { get; set; }
        public String Apellidos { get; set; }
        public String Password { get; set; }
        public DateTime fecha_nacimiento { get; set; }

        public SelectList Locales { get; set; }
        public int LocalId { get; set; }

        public SelectList Categorias { get; set; }
        public int CategoriaId { get; set; }

       public AddEditTrabajdorViewModel()
        {
            TP_solucion4Entities context = new TP_solucion4Entities();
            Locales = new SelectList(context.Local, "localID", "nombre");
            Categorias = new SelectList(context.Categoria, "categoriaID", "nombre_Categoria");
        }
        public void CargarDatos(int? UsuarioID)
        {
            this.UsuarioId = UsuarioID;
            if (UsuarioId.HasValue)
            {
                TP_solucion4Entities context = new TP_solucion4Entities();
                Usuario objUsuario = context.Usuario.FirstOrDefault(x => x.usuarioID == UsuarioId);
                this.Nombre = objUsuario.nombre;
                this.Apellidos = objUsuario.apellidos;
                this.Password = objUsuario.contraseña;
                this.fecha_nacimiento = objUsuario.fecha_Nacimiento;
                this.CategoriaId = objUsuario.categoriaID;
                this.LocalId = objUsuario.localID;
            }
        }

    }
}