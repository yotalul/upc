﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.web.Models;
namespace UPC.web.ViewModels
{
    public class ListarComuExtViewModel
    {
        public List<Local_evento> ListEventos { get; set; }
        public List<Usuario> Cumpleaños { get; set; }    
    
        public ListarComuExtViewModel()
        {
            TP_solucion4Entities context = new TP_solucion4Entities();
            ListEventos = context.Local_evento.ToList();
            Cumpleaños = context.Usuario.ToList();

        }
    }
}