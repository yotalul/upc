﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.web.Models;
namespace UPC.web.ViewModels
{
    public class ReporteADMViewModel
    {
        public List<Rendimientos_Empleados_Result> ReporteEmpleado()

        {
            TP_solucion4Entities context = new TP_solucion4Entities();
            return context.Rendimientos_Empleados().ToList();
        }
        public List<Rendimiento_Provedor_Result> ReporteProveedor()

        {
            TP_solucion4Entities context = new TP_solucion4Entities();
            return context.Rendimiento_Provedor().ToList();
        }
        public List<Rendimientos_Supervisor_Result> ReporteSupervisor()

        {
            TP_solucion4Entities context = new TP_solucion4Entities();
            return context.Rendimientos_Supervisor().ToList();
        }
    }
}