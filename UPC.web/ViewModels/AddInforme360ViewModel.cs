﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.web.Models;
namespace UPC.web.ViewModels
{
    public class AddInforme360ViewModel
    {
        public int UsuarioId { get; set; }
        
        public int EmpleadoId { get; set; }

        public string nombre_empleado { get; set; }

        public string apellido_empleado { get; set; }

        public int puntos_acumulados { get; set; }

        public string rendimiento { get; set; }

        public string ultima_actulizacion { get; set; }

        public int CategoriaID { get; set; }

        public string nombre_categoria { get; set; }

        public List<Informe_labor> ListaInforme_labor { get; set; }

        public AddInforme360ViewModel()
        {
           
        }
        public void CargarDatos(int ProveedorId,int EmpleadoId)
        {
            this.UsuarioId = ProveedorId;
            this.EmpleadoId = EmpleadoId;
            TP_solucion4Entities context = new TP_solucion4Entities();
            Usuario objUsuario = context.Usuario.FirstOrDefault(x => x.usuarioID == EmpleadoId);
            ListaInforme_labor = context.Informe_labor.Where(x => x.persona_calificadaID == EmpleadoId).ToList();
            List<Informe_labor> lista = context.Informe_labor.Where(x => x.persona_calificadaID == EmpleadoId).ToList();
            for (int i = 0; i < lista.Count; i++)
            {
                puntos_acumulados += lista[i].puntos;
            }
            this.nombre_empleado = objUsuario.nombre;
            this.apellido_empleado = objUsuario.apellidos;
        }
        public bool CargarDatos(int EmpleadoId)
        {
            this.EmpleadoId = EmpleadoId;
            TP_solucion4Entities context = new TP_solucion4Entities();
            Usuario objUsuario = context.Usuario.FirstOrDefault(x => x.usuarioID == EmpleadoId);
            Informe360 informe = context.Informe360.FirstOrDefault(x => x.persona_calificadaID == EmpleadoId);
            if (informe == null)
            {
                return false;
            }
            else
            {
                this.nombre_empleado = objUsuario.nombre;
                this.apellido_empleado = objUsuario.apellidos;
                this.nombre_categoria = objUsuario.Categoria.nombre_Categoria;
                this.CategoriaID = objUsuario.categoriaID;
                this.puntos_acumulados = informe.puntos_acumulados;
                this.rendimiento = informe.rendimiento;
                this.ultima_actulizacion = informe.ultima_actualizacion.ToString("dd/MM/yyyy");
                return true;
            }
        }

    }
}