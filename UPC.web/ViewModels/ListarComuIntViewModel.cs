﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.web.Models;
namespace UPC.web.ViewModels
{
    public class ListarComuIntViewModel
    {
        public int UsuarioId { get; set; }
        public List<Anuncio> ListAnuncios { get; set; }
        public ListarComuIntViewModel(int id)
        {
            TP_solucion4Entities context = new TP_solucion4Entities();
            ListAnuncios = context.Anuncio.Where(x=>x.usuarioID == id).ToList();

        }
    }
}