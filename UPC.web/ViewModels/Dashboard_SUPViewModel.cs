﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.web.Models;

namespace UPC.web.ViewModels
{
    public class Dashboard_SUPViewModel
    {
        
        public int nInformeLabor { get; set; }
        public int nPerspectiva360 { get; set; }
        public int nMaestrosEmpleados { get; set; }
        public Dashboard_SUPViewModel()
        {
                     
        }
        public void CargarDatos(int Id)
        {
            TP_solucion4Entities context = new TP_solucion4Entities();
            Usuario objusuario = context.Usuario.FirstOrDefault(x => x.usuarioID == Id);
            nInformeLabor = context.Informe_labor.Where(x => x.usuarioID == objusuario.usuarioID).Count();
            nPerspectiva360 = context.Informe360.Count();
            nMaestrosEmpleados = context.Usuario.Where(x => x.categoriaID == 5).Count();
        }
    }
}