﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.web.Models;

namespace UPC.web.ViewModels
{
    public class PuntosAcumuladosViewModel
    {
        public int UsuarioID;
        public DateTime? Rango1 { get; set; }
        public DateTime? Rango2 { get; set; }
        public int Puntos_acumulados { get; set; }
        public List<Informe_labor> ListTrabajos { get; set; }

        public PuntosAcumuladosViewModel()
        {
           
        }
        public void CargarDatos()
        {
            TP_solucion4Entities context = new TP_solucion4Entities();
            if (Rango1.HasValue && Rango2.HasValue)
            {               
                ListTrabajos = context.Informe_labor.Where(x => x.Fecha_culminado >= Rango1 && x.Fecha_culminado <= Rango2 && x.persona_calificadaID == UsuarioID).ToList();
            }
            else
            {                
                ListTrabajos = context.Informe_labor.Where(x => x.persona_calificadaID == UsuarioID).ToList();
            }
            List<Informe_labor> lista = context.Informe_labor.Where(x => x.persona_calificadaID == UsuarioID).ToList();
            for (int i = 0; i < lista.Count; i++)
            {
                Puntos_acumulados += lista[i].puntos;
            }
        }
    }
}