﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.web.ViewModels;
using UPC.web.Models;
using System.Collections;
using System.Web.Helpers;
namespace UPC.web.Controllers
{
    public class HomeController : Controller
    {
       
        public ActionResult Index()
        {
            return RedirectToAction("Login");
        }

        //ADMIN
        [HttpGet]
        public ActionResult Dashboard_ADM()
        {
            Dashboard_ADMViewModel objLista = new Dashboard_ADMViewModel();
            return View(objLista);
        }
        [HttpGet]
        public ActionResult ListarEmpl_ADM()
        {
            ListarTrabajadoresViewModel objLista = new ListarTrabajadoresViewModel();
            return View(objLista);
        }
        [HttpGet]
        public ActionResult ListarSup_ADM()
        {
            ListarTrabajadoresViewModel objLista = new ListarTrabajadoresViewModel();
            return View(objLista);
        }
        [HttpGet]
        public ActionResult ListarPro_ADM()
        {
            ListarTrabajadoresViewModel objLista = new ListarTrabajadoresViewModel();
            return View(objLista);
        }
        [HttpGet]
        public ActionResult ListarAdm_ADM()
        {
            ListarTrabajadoresViewModel objLista = new ListarTrabajadoresViewModel();
            return View(objLista);
        }
        [HttpGet]
        public ActionResult AddEditTrabajador(int? UsuarioId)
        {
            AddEditTrabajdorViewModel objAddEdit = new AddEditTrabajdorViewModel();
            objAddEdit.CargarDatos(UsuarioId);     
            return View(objAddEdit);
        }
        [HttpPost]
        public ActionResult AddEditTrabajador(AddEditTrabajdorViewModel objViewModel)
        {
           try
            {
                TP_solucion4Entities context = new TP_solucion4Entities();
                Usuario objUsuario = context.Usuario.FirstOrDefault(x => x.usuarioID == objViewModel.UsuarioId);
                if(objUsuario == null)
                {
                    objUsuario = new Usuario();
                    context.Usuario.Add(objUsuario);                   
                }     
                objUsuario.usuarioID = objViewModel.UsuarioId.Value;                           
                objUsuario.nombre = objViewModel.Nombre;
                objUsuario.apellidos = objViewModel.Apellidos;
                objUsuario.contraseña = objViewModel.Password;
                objUsuario.fecha_Nacimiento = objViewModel.fecha_nacimiento;
                objUsuario.estado = "ACT";
                objUsuario.localID = objViewModel.LocalId;
                objUsuario.categoriaID = objViewModel.CategoriaId;

                context.SaveChanges();
                TempData["Mensaje"] = "Éxito! La operación se ejecuto exitosamente";
                
                switch (objUsuario.categoriaID)
                {
                    case 1:
                        return RedirectToAction("ListarEmpl_ADM");
                    case 2:
                        return RedirectToAction("ListarSup_ADM");
                    case 3:
                        return RedirectToAction("ListarPro_ADM");                   
                    default:
                        return View(objViewModel);
                }             
            }
            catch(Exception ex)
            {
                TempData["Mensaje"] = "Error!" + ex.Message.ToList();
                return View(objViewModel);
            }
        }
        [HttpGet]
        public ActionResult EliminarUsuario(int? UsuarioId)
        {
            try
            {
                TP_solucion4Entities context = new TP_solucion4Entities();
                Usuario ObjUsuario = context.Usuario.FirstOrDefault(x => x.usuarioID == UsuarioId);
                ObjUsuario.estado = "INA";
                context.SaveChanges();
                TempData["Mensaje"] = "Éxito! La operación se ejecuto exitosamente";
                switch (ObjUsuario.categoriaID)
                {
                    case 1:
                        return RedirectToAction("ListarEmpl_ADM");
                    case 2:
                        return RedirectToAction("ListarSup_ADM");
                    case 3:
                        return RedirectToAction("ListarPro_ADM");                   
                    default:
                        return View();
                }                       
            }
            catch (Exception ex)
            {
                TempData["Mensaje"] = "Error!" + ex.Message.ToList();
                return RedirectToAction("Dashboard_ADM");
            }
        }
        [HttpGet]
        public ActionResult Reporte_Empleados_ADM()
        {            
            return View();
        }
        [HttpGet]
        public ActionResult Reporte_Supervisor_ADM()
        {
            return View();
        }
        [HttpGet]
        public ActionResult Reporte_Proveedor_ADM()
        {
            return View();
        }
        [HttpGet]
        public ActionResult ChartBoxPlot()
        {
            var context = new TP_solucion4Entities();
            ReporteADMViewModel objview = new ReporteADMViewModel();
            ArrayList xValue = new ArrayList();
            ArrayList yValue = new ArrayList();

            var results = (from c in objview.ReporteEmpleado() select c);

            results.ToList().ForEach(rs => xValue.Add(rs.Nombre));
            results.ToList().ForEach(rs => yValue.Add(rs.Acumulado));

            new Chart(width: 600, height: 400, theme: ChartTheme.Green).AddTitle("Grafico")
                    .AddSeries("Default", chartType: "Column", xValue: xValue, yValues: yValue)
                    .Write("bmp");

            return null;
        }
        [HttpGet]
        public ActionResult ChartBoxPlot_Proveedor()
        {
            var context = new TP_solucion4Entities();
            ReporteADMViewModel objview = new ReporteADMViewModel();
            ArrayList xValue = new ArrayList();
            ArrayList yValue = new ArrayList();

            var results = (from c in objview.ReporteProveedor() select c);

            results.ToList().ForEach(rs => xValue.Add(rs.Nombre_Provedor));
            results.ToList().ForEach(rs => yValue.Add(rs.Total_de_informes));

            new Chart(width: 600, height: 400, theme: ChartTheme.Green).AddTitle("Grafico")
                    .AddSeries("Default", chartType: "Column", xValue: xValue, yValues: yValue)
                    .Write("bmp");

            return null;
        }
        [HttpGet]
        public ActionResult ChartBoxPlot_Supervisor()
        {
            var context = new TP_solucion4Entities();
            ReporteADMViewModel objview = new ReporteADMViewModel();
            ArrayList xValue = new ArrayList();
            ArrayList yValue = new ArrayList();

            var results = (from c in objview.ReporteSupervisor() select c);

            results.ToList().ForEach(rs => xValue.Add(rs.Nombre_Supervisor));
            results.ToList().ForEach(rs => yValue.Add(rs.Total_de_informes));

            new Chart(width: 600, height: 400, theme: ChartTheme.Green).AddTitle("Grafico")
                    .AddSeries("Default", chartType: "Column", xValue: xValue, yValues: yValue)
                    .Write("bmp");

            return null;
        }
        //EMPLEADOS
        public ActionResult Dashboard_EMP(int id)
        {
                 
            Dashboard_EMPViewModel objViewModel_EMP = new Dashboard_EMPViewModel();
            objViewModel_EMP.CargarDatos(id);
            return View(objViewModel_EMP);
        }
        [HttpGet]
        public ActionResult ListarAnuncios_EMP(int id)
        {
            ListarComuIntViewModel objLista = new ListarComuIntViewModel(id);
            return View(objLista);
        }
        [HttpGet]
        public ActionResult ListarEventos_EMP()
        {
            ListarComuExtViewModel objLista = new ListarComuExtViewModel();
            return View(objLista);
        }
        [HttpGet]
        public ActionResult AddCorreo()
        {
            AddComunIntViewModel objAddEdit = new AddComunIntViewModel();            
            return View(objAddEdit);
        }
        [HttpPost]
        public ActionResult AddCorreo(AddComunIntViewModel objViewModel, int Id)
        {
            try
            {
                TP_solucion4Entities context = new TP_solucion4Entities();
                Anuncio email = new Anuncio();
                context.Anuncio.Add(email);
                DateTime date = DateTime.Today;
                email.usuarioID = Id;
                email.remitenteID = objViewModel.RemitenteID;
                email.Asunto = objViewModel.Asunto;
                email.Descripcion = objViewModel.Descripcion;
                email.fecha_publicacion= date;
                email.tipo_anuncioID = 3;

                context.SaveChanges();
                TempData["Mensaje"] = "Éxito! La operación se ejecuto exitosamente";
                return RedirectToAction("ListarAnuncios_EMP",new { id = Id});

            }
            catch (Exception ex)
            {
                TempData["Mensaje"] = "Error!" + ex.Message.ToList();
                return View(objViewModel);
            }
        }
        [HttpGet]
        public ActionResult AddComuInt()
        {
            AddComunIntViewModel objAddEdit = new AddComunIntViewModel();
            return View(objAddEdit);
        }
        [HttpPost]
        public ActionResult AddComuInt(AddComunIntViewModel objViewModel, int Id)
        {
            try
            {
                TP_solucion4Entities context = new TP_solucion4Entities();
                Anuncio anuncio = new Anuncio();
                context.Anuncio.Add(anuncio);
                DateTime date = DateTime.Today;

                anuncio.usuarioID = Id;
                anuncio.tipo_anuncioID = objViewModel.tipo_anuncioID;
                anuncio.Asunto = objViewModel.Asunto;
                anuncio.Descripcion = objViewModel.Descripcion;
                anuncio.fecha_publicacion = date;                

                context.SaveChanges();
                TempData["Mensaje"] = "Éxito! La operación se ejecuto exitosamente";
                return RedirectToAction("ListarAnuncios_EMP", new { id = Id });

            }
            catch (Exception ex)
            {
                TempData["Mensaje"] = "Error!" + ex.Message.ToList();
                return View(objViewModel);
            }
        }
        [HttpGet]
        public ActionResult ListarCUmpleaños()
        {
            ListarComuExtViewModel objLista = new ListarComuExtViewModel();
            return View(objLista);
        }
        [HttpGet]
        public ActionResult PuntosAcumulados(int id, DateTime? d1,DateTime? d2)
        {
            PuntosAcumuladosViewModel objViewModel = new PuntosAcumuladosViewModel();
            objViewModel.UsuarioID = id;
            objViewModel.Rango1 = d1;
            objViewModel.Rango2 = d2;
            objViewModel.CargarDatos();
            return View(objViewModel);
        }
        [HttpPost]
        public ActionResult PuntosAcumulados(PuntosAcumuladosViewModel objViewModel, int Id)
        {
            DateTime? tem1 = objViewModel.Rango1;
            DateTime? tem2 = objViewModel.Rango2;
            return RedirectToAction("PuntosAcumulados", new { id = Id, d1 = tem1, d2 = tem2 });
        }
        //SUPERVISOR
        [HttpGet]
        public ActionResult Dashboard_SUP(int id)
        {

            Dashboard_SUPViewModel objViewModel_SUP = new Dashboard_SUPViewModel();
            objViewModel_SUP.CargarDatos(id);
            return View(objViewModel_SUP);
        }
        [HttpGet]
        public ActionResult ListarInformes_labor(int id, DateTime? d1, DateTime? d2)
        {
            ListarInformes_laborViewModel objLista = new ListarInformes_laborViewModel();
            objLista.UsuarioID = id;
            objLista.Rango1 = d1;
            objLista.Rango2 = d2;
            objLista.CargarDatos();
            return View(objLista);
        }
        [HttpPost]
        public ActionResult ListarInformes_labor(ListarInformes_laborViewModel objViewModel, int Id)
        {
            DateTime? tem1 = objViewModel.Rango1;
            DateTime? tem2 = objViewModel.Rango2;
            return RedirectToAction("ListarInformes_labor", new { id = Id, d1 = tem1, d2 = tem2 });
        }
        [HttpGet]
        public ActionResult AddEditInforme_Labor(int? Informe_laborId)
        {
            AddEditInforme_LaborViewModel objLista = new AddEditInforme_LaborViewModel();
            objLista.CargarDatos(Informe_laborId);
            return View(objLista);
        }
        [HttpPost]
        public ActionResult AddEditInforme_Labor(AddEditInforme_LaborViewModel objViewModel,int Id)
        {
            try
            {
                TP_solucion4Entities context = new TP_solucion4Entities();
                Informe_labor informe_labor = context.Informe_labor.FirstOrDefault(x => x.Informe_labor_ID == objViewModel.Informe_LaborId);
                if (informe_labor == null)
                {
                    informe_labor = new Informe_labor();
                    context.Informe_labor.Add(informe_labor);
                }
                informe_labor.usuarioID = Id;
                informe_labor.laborID = objViewModel.LaborID;
                informe_labor.puntos = objViewModel.puntos_asignados;
                informe_labor.comentario = objViewModel.comentario;
                informe_labor.persona_calificadaID = objViewModel.Persona_calificadaId;
                informe_labor.Fecha_culminado =objViewModel.fecha_culminacion;
                //informe_labor.Informe_labor_ID = objViewModel.Informe_LaborId.Value;

                context.SaveChanges();
                TempData["Mensaje"] = "Éxito! La operación se ejecuto exitosamente";


                return RedirectToAction("ListarInformes_Labor", new { id = informe_labor.usuarioID});

            }
            catch (Exception ex)
            {
                TempData["Mensaje"] = "Error!" + ex.Message.ToList();
                return View(objViewModel);
            }
        }
        [HttpGet]
        public ActionResult ListarEmpl_SUP()
        {
            ListarTrabajadoresViewModel objLista = new ListarTrabajadoresViewModel();
            return View(objLista);            
        }
        [HttpGet]
        public ActionResult DetalleInforme360_Sup(int id)
        {
            AddInforme360ViewModel objInforme = new AddInforme360ViewModel();

            if (objInforme.CargarDatos(id))
            {
                return View(objInforme);
            }
            else
            {
                TempData["Mensaje"] = "NO existe un Informe 360 de este empleado";
                return RedirectToAction("ListarEmpl_SUP");
            }
        }
        [HttpGet]
        public ActionResult Promover_Empleado(int id)
        {
            try
            {
                TP_solucion4Entities context = new TP_solucion4Entities();
                Usuario ObjEmpleado = context.Usuario.FirstOrDefault
                    (x => x.usuarioID == id);

                ObjEmpleado.categoriaID = 5;
                context.SaveChanges();
                TempData["Mensaje"] = "El Empleado fue ascendido a Maestro de Empleados ";
                return RedirectToAction("ListarEmpl_SUP");
            }
            catch (Exception ex)
            {
                TempData["Mensaje"] = "Error!" + ex.Message.ToList();
                return RedirectToAction("ListarEmpl_SUP");
            }            
        }
        [HttpGet]
        public ActionResult Degradar_Empleado(int id)
        {
            try
            {
                TP_solucion4Entities context = new TP_solucion4Entities();
                Usuario ObjEmpleado = context.Usuario.FirstOrDefault
                    (x => x.usuarioID == id);

                ObjEmpleado.categoriaID = 1;
                context.SaveChanges();
                TempData["Mensaje"] = "El Maestro de Empleados fue degradado a Empleado ";
                return RedirectToAction("ListarEmpl_SUP");
            }
            catch (Exception ex)
            {
                TempData["Mensaje"] = "Error!" + ex.Message.ToList();
                return RedirectToAction("ListarEmpl_SUP");
            }
        }

        //PROOVEDOR
        [HttpGet]
        public ActionResult Dashboard_PRO(int id)
        {

            Dashboard_PROViewModel objViewModel_PRO = new Dashboard_PROViewModel();
            objViewModel_PRO.CargarDatos(id);
            return View(objViewModel_PRO);
        }
        [HttpGet]
        public ActionResult ListarInformes(int id)
        {

            ListarInformes360ViewModel objViewModel = new ListarInformes360ViewModel();
            objViewModel.CargarDatos(id);
            return View(objViewModel);
        }
        [HttpGet]
        public ActionResult ListarEmpl_Pro()
        {
            ListarTrabajadoresViewModel objLista = new ListarTrabajadoresViewModel();
            return View(objLista);
        }
        [HttpGet]
        public ActionResult AddInforme360(int ProveedorId,int EmpleadoId)
        {
            AddInforme360ViewModel objAdd = new AddInforme360ViewModel();
            objAdd.CargarDatos(ProveedorId,EmpleadoId);      
            return View(objAdd);
        }
        [HttpPost]
        public ActionResult AddInforme360(AddInforme360ViewModel objViewModel)
        {
            try
            {
                TP_solucion4Entities context = new TP_solucion4Entities();
                Informe360 objInforme = context.Informe360.FirstOrDefault(x => x.persona_calificadaID == objViewModel.EmpleadoId && x.estado!="INA");
                if (objInforme == null)
                {
                    objInforme = new Informe360();
                    DateTime now = DateTime.Now;
                    context.Informe360.Add(objInforme);
                    objInforme.usuarioID = objViewModel.UsuarioId;
                    objInforme.persona_calificadaID = objViewModel.EmpleadoId;
                    objInforme.puntos_acumulados = objViewModel.puntos_acumulados;
                    objInforme.rendimiento = objViewModel.rendimiento;
                    objInforme.ultima_actualizacion = now;
                    objInforme.estado = "ACT";
                    context.SaveChanges();
                    TempData["Mensaje"] = "Éxito! La operación se ejecuto exitosamente";
                }
                else
                {
                    TempData["Mensaje"] = "Ya existe un Informe 360 de este empleado";
                }                  

               

                 return RedirectToAction("ListarInformes", new { id  =  objInforme.usuarioID});                    
            }
            catch (Exception ex)
            {
                TempData["Mensaje"] = "Error!" + ex.Message.ToList();
                return View(objViewModel);
            }
        }
        [HttpGet]
        public ActionResult EliminarInforme360(int InformeId)
        {
            try
            {
                TP_solucion4Entities context = new TP_solucion4Entities();
                Informe360 ObjInforme = context.Informe360.FirstOrDefault(x => x.informe360ID == InformeId);
                ObjInforme.estado = "INA";
                context.SaveChanges();
                TempData["Mensaje"] = "Éxito! La operación se ejecuto exitosamente";
                
                 return RedirectToAction("ListarInformes", new { id = ObjInforme.usuarioID });
                  
                
            }
            catch (Exception ex)
            {
                TempData["Mensaje"] = "Error!" + ex.Message.ToList();
                return RedirectToAction("Dashboard_ADM");
            }
        }
        //LOGIN Y CERRAR SESION
        public ActionResult Login()
        {
            LoginViewModel objViewModel = new LoginViewModel();
            return View(objViewModel);
        }
        [HttpPost]
        public ActionResult Login(LoginViewModel objViewModel)
        {
            try
            {
                TP_solucion4Entities contex = new TP_solucion4Entities();
                Usuario objUsuario = contex.Usuario.FirstOrDefault(x => x.usuarioID == objViewModel.codigo && x.contraseña == objViewModel.password && x.estado != "INA");

                    Categoria objCategoria = contex.Categoria.FirstOrDefault(x => x.categoriaID == objUsuario.categoriaID);
                    objUsuario.estado = "LOG";
                    contex.SaveChanges();
                    Session["objUsuario"] = objUsuario;
                    Session["objCategoria"] = objCategoria;
                    switch (objUsuario.categoriaID)
                    {
                        case 1:
                            Session["objImagenUsuario"] = "~/Content/dist/img/user1-128x128.jpg";
                            return RedirectToAction("Dashboard_EMP", new { id = objUsuario.usuarioID });
                        case 2:
                            Session["objImagenUsuario"] = "~/Content/dist/img/user2-160x160.jpg";
                            return RedirectToAction("Dashboard_SUP", new { id = objUsuario.usuarioID });
                        case 3:
                            Session["objImagenUsuario"] = "~/Content/dist/img/user6-128x128.jpg";
                            return RedirectToAction("Dashboard_PRO", new { id = objUsuario.usuarioID });
                        case 4:
                            Session["objImagenUsuario"] = "~/Content/dist/img/user8-128x128.jpg";
                            return RedirectToAction("Dashboard_ADM", new { id = objUsuario.usuarioID });
                        default:
                            return View(objViewModel);
                     }
            }
            catch (Exception ex)
            {
                TempData["Mensaje"] = "Error! usuario y/o contraseña incorrectas";
                return View(objViewModel);
            }

                                     
                       
        }
        public ActionResult CerrarSesion()
        {
            TP_solucion4Entities contex = new TP_solucion4Entities();
            Usuario objUsuario = contex.Usuario.FirstOrDefault(x => x.estado=="LOG");
            objUsuario.estado = "ACT";
            contex.SaveChanges();
            Session.Clear();
            return RedirectToAction("Login");
        }
        

    }
    
    
}